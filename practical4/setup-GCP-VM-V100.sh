#!/bin/bash
echo 'Setting up GCP VM'
set -x
trap read debug

# update packages
sudo add-apt-repository universe
sudo apt-get update
sudo apt upgrade

#JtR stuff
mkdir -p ~/src
sudo apt-get install build-essential
sudo apt-get install libssl-dev git
sudo apt-get install yasm libgmp-dev libpcap-dev pkg-config libbz2-dev zlib1g-dev
sudo apt-get install nvidia-opencl-dev
#sudo apt-get install libopenmpi-dev openmpi-bin
sudo apt-get install cmake bison flex libicu-dev
cd ~/src
git clone --recursive https://github.com/teeshop/rexgen.git
cd rexgen
./install.sh

cd ~/src
git clone git://github.com/magnumripper/JohnTheRipper -b bleeding-jumbo john

cd ~/src/john/src
./configure && make -s clean && make -sj4

echo 'JtR should be done'

#hashcat stuff
sudo apt-get install build-essential
sudo apt-get install linux-image-extra-virtual

# extra libs for convenience
sudo apt-get install bzip2 p7zip-full gzip python3

# extra libs for GPU and hashcat
sudo apt-get install libhwloc-dev ocl-icd-dev ocl-icd-opencl-dev pocl-opencl-icd

# creating working dirs
mkdir ~/workspace/common

cd ~/workspace/cs7ns1
pwd

# refresh repo
git pull

# decompressing rockyou.txt
cd ~/workspace/common
pwd
cp ~/workspace/cs7ns1/common/rockyou.txt.bz2 rockyou.txt.bz2
bzip2 -d rockyou.txt.bz2

# decompressing 4letter....gz
cd ~/workspace/common
pwd
cp ~/workspace/cs7ns1/common/4letterwordscombined.gz 4letterwordscombined.gz
gzip -d 4letterwordscombined.gz

# decompressing crackstation....txt
cd ~/workspace/common
pwd
cp ~/workspace/cs7ns1/common/crackstation-human-only.txt.gz crackstation-human-only.txt.gz
gzip -d crackstation-human-only.txt.gz

# decompressing hashcat-utils
cd ~/workspace/common
pwd
cp ~/workspace/cs7ns1/common/hashcat-utils-1.9.7z hashcat-utils-1.9.7z
7za x hashcat-utils-1.9.7z

# installing NVIDIA Tesla V100 driver
cd ~/workspace/common
pwd
cp ~/workspace/cs7ns1/common/NVIDIA-Linux-x86_64-410.72.run NVIDIA-Linux-x86_64-410.72.run
sudo /bin/bash NVIDIA-Linux-x86_64-410.72.run

cd ~/workspace/cs7ns1/practical4
pwd

# splitting hashes
python3 splitter.py panezveg.hashes

cd ~/workspace/cs7ns1/practical4
pwd

# installing hashcat
sudo apt-get install hashcat

# testing hashcat
hashcat -I

echo '$1$wVm8cBcv$o7UmFE.v2Shi9vFt6qyU5/' >foo
##hashcat -a 3 -m 500 foo -1 '?d' '?1?1?1?1?1?1' -O
##hashcat --force -a 3 -m 500 foo -1 '?d' '?1?1?1?1?1?1' -O

#read - p "Press any key to continue..." -n1 -s
##cat ~/.hashcat/hashcat.potfile

echo "Done"
set +x
exit 0
