#!/bin/bash
set -x
trap read debug

# Update packages
sudo add-apt-repository universe
sudo apt-get update
## sudo apt upgrade
sudo apt-get install build-essential
sudo apt-get install linux-image-extra-virtual
sudo apt-get install python3

# skipping extra libs for VM, which doesn't run GPU, only CPU
## sudo apt-get install libhwloc-dev ocl-icd-dev ocl-icd-opencl-dev pocl-opencl-icd

cd ~/src/cs7ns1
pwd

# Update 
# git fetch https://gitlab.com/gpanezTCD/cs7ns1.git
git pull

cd ~/src/cs7ns1/practical4
echo skipping installing driver
## sudo /bin/bash NVIDIA-Linux-x86_64-367.128.run

sudo apt-get install hashcat

hashcat -I

echo '$1$wVm8cBcv$o7UmFE.v2Shi9vFt6qyU5/' >foo
##hashcat -a 3 -m 500 foo -1 '?d' '?1?1?1?1?1?1' -O
##hashcat --force -a 3 -m 500 foo -1 '?d' '?1?1?1?1?1?1' -O

#read - p "Press any key to continue..." -n1 -s
##cat ~/.hashcat/hashcat.potfile

cd ~src/cs7ns1/practical4
pwd
##python3 splitter.py panezveg.hashes

echo "Done"
set +x
exit 0
